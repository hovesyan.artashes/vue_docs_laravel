<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Doc;

class DocController extends Controller
{
  public function index(){
    $docCount = 7;
    $docs = Doc::paginate($docCount);
    return $docs;
  }

  public function store(Request $request){
    $doc = new Doc;
    $doc->title = $request->title;
    $doc->content = $request->content;
    $doc->save();
    return $doc;
  }

  public function delete(Doc $doc){
    $doc = Doc::find($doc->id);
    $doc->delete();
    return $doc;
  }

  public function edit(Request $request){
    $doc = Doc::find($request->id);
    $doc->title = $request->title;
    $doc->content = $request->content;
    $doc->save();
    return $doc;
  }
}
